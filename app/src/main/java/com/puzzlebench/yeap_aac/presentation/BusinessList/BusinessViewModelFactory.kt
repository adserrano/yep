package com.puzzlebench.yeap_aac.presentation.BusinessList

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.puzzlebench.yeap_aac.repository.BusinessRepository

@Suppress("UNCHECKED_CAST")
class BusinessViewModelFactory(private val businessRepository: BusinessRepository)
    :ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        BusinessViewModel(businessRepository) as T
}