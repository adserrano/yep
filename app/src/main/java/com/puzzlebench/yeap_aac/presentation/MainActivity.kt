package com.puzzlebench.yeap_aac.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.puzzlebench.yeap_aac.R
import com.puzzlebench.yeap_aac.YepApplication
import com.puzzlebench.yeap_aac.presentation.BusinessDetails.BusinessDetailsViewModel
import com.puzzlebench.yeap_aac.presentation.di.ViewModelInjector
import com.puzzlebench.yeap_aac.presentation.BusinessList.BusinessViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val viewModel: BusinessViewModel by viewModels {
        ViewModelInjector.provideBusinessViewModel(this.applicationContext as YepApplication)
    }
    private val viewModel2: BusinessDetailsViewModel by viewModels {
        ViewModelInjector.provideBusinessDetailsViewModel(this.applicationContext as YepApplication)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.getBusinessList()
        viewModel.business.observeForever {
            textView.text = it
        }
        viewModel2.getBusinessDetails("KeNGoOn5jsAtsq-AXyDWzQ")
        viewModel2.business.observeForever {
            textView2.text = it.first
            textView3.text = it.second
        }
    }
}
