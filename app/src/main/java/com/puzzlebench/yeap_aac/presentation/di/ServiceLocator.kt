package com.puzzlebench.yeap_aac.presentation.di

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.puzzlebench.yeap_aac.data.local.BusinessDetailsLocalData
import com.puzzlebench.yeap_aac.data.local.BusinessDetailsLocalDataImpl
import com.puzzlebench.yeap_aac.data.local.BusinessLocalData
import com.puzzlebench.yeap_aac.data.local.BusinessLocalDataImpl
import com.puzzlebench.yeap_aac.data.local.room.YepDataBase
import com.puzzlebench.yeap_aac.data.mapper.BusinessDetailMapper
import com.puzzlebench.yeap_aac.data.mapper.BusinessMapper
import com.puzzlebench.yeap_aac.data.network.FetchBusinessDetailsByIdImpl
import com.puzzlebench.yeap_aac.data.network.FetchSwitzerlandBusinesses
import com.puzzlebench.yeap_aac.data.network.FetchSwitzerlandBusinessesImpl
import com.puzzlebench.yeap_aac.data.network.retrofit.RetrofitClient
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepository
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepositoryImpl
import com.puzzlebench.yeap_aac.repository.BusinessRepository
import com.puzzlebench.yeap_aac.repository.BusinessRepositoryImpl

object ServiceLocator {

    private var database: YepDataBase? = null
    @Volatile
    var businessRepository: BusinessRepository? = null
        @VisibleForTesting set

    fun provideBusinessRepository(context: Context): BusinessRepository {
        synchronized(this) {
            return businessRepository
                ?: createBusinessRepository(
                    context
                )
        }
    }

    @Volatile
    var dbusinessDetailsRepository: BusinessDetailsRepository? = null
        @VisibleForTesting set

    fun provideBusinessDetailsRepository(context: Context): BusinessDetailsRepository {
        synchronized(this) {
            return dbusinessDetailsRepository
                ?: createBusinessDetailsRepository(
                    context
                )
        }
    }

    private fun createBusinessDetailsRepository(context: Context): BusinessDetailsRepository {
        val businessDetailsRepositoryImpl =
            BusinessDetailsRepositoryImpl(
                provideFetchBusinessDetailsById(),
                createBusinessDetailsLocalData(context)
            )
        dbusinessDetailsRepository = businessDetailsRepositoryImpl
        return businessDetailsRepositoryImpl
    }

    private fun createBusinessRepository(context: Context): BusinessRepository {
        val businessRepositoryImpl =
            BusinessRepositoryImpl(
                provideFetchSwitzerlandBusiness(),
                createBusinessLocalDataSource(
                    context
                )
            )
        businessRepository = businessRepositoryImpl
        return businessRepositoryImpl
    }

    private fun provideYelpApiV3() = RetrofitClient.makeServiceYelpApiV3()
    private fun provideBusinessMapper() = BusinessMapper()
    private fun provideBusinessDetailMapper() = BusinessDetailMapper()

    private fun provideFetchBusinessDetailsById() =
        FetchBusinessDetailsByIdImpl(
            provideYelpApiV3(),
            provideBusinessDetailMapper()
        )

    private fun provideFetchSwitzerlandBusiness(): FetchSwitzerlandBusinesses {
        return FetchSwitzerlandBusinessesImpl(
            provideYelpApiV3(),
            provideBusinessMapper()
        )
    }


    private fun createBusinessLocalDataSource(context: Context): BusinessLocalData {
        val database = database
            ?: createDataBase(context)
        return BusinessLocalDataImpl(
            database.businessDao(),
            BusinessMapper()
        )
    }

    private fun createBusinessDetailsLocalData(context: Context): BusinessDetailsLocalData {
        val database = database
            ?: createDataBase(context)
        return BusinessDetailsLocalDataImpl(
            database.categoriesDao(),
            database.photoDao(),
            provideBusinessDetailMapper()
        )
    }

    private fun createDataBase(context: Context): YepDataBase {
        val result = Room.databaseBuilder(
            context.applicationContext,
            YepDataBase::class.java, "Yep.db"
        ).build()
        database = result
        return result
    }

}