package com.puzzlebench.yeap_aac.presentation.di

import com.puzzlebench.yeap_aac.YepApplication
import com.puzzlebench.yeap_aac.data.mapper.BusinessDetailMapper
import com.puzzlebench.yeap_aac.data.mapper.BusinessMapper
import com.puzzlebench.yeap_aac.data.network.FetchBusinessDetailsByIdImpl
import com.puzzlebench.yeap_aac.data.network.FetchSwitzerlandBusinessesImpl
import com.puzzlebench.yeap_aac.data.network.retrofit.RetrofitClient
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepositoryImpl
import com.puzzlebench.yeap_aac.repository.BusinessRepositoryImpl
import com.puzzlebench.yeap_aac.presentation.BusinessDetails.BusinessDetailsViewModelFactory
import com.puzzlebench.yeap_aac.presentation.BusinessList.BusinessViewModelFactory

object ViewModelInjector {

    fun provideBusinessViewModel(app: YepApplication) =
        BusinessViewModelFactory(
            app.businessRepository
        )

    fun provideBusinessDetailsViewModel(app: YepApplication) =
        BusinessDetailsViewModelFactory(
            app.businessDetailsRepository
        )
}