package com.puzzlebench.yeap_aac.presentation.BusinessDetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepository
import kotlinx.coroutines.launch

class BusinessDetailsViewModel constructor(private val businessDetailsRepository: BusinessDetailsRepository) :
    ViewModel() {

    val business: MutableLiveData<Pair<String, String>> = MutableLiveData()

    fun getBusinessDetails(businessId: String) {
        viewModelScope.launch {
            val businessDetailsData = businessDetailsRepository.getBusinessDetailsById(businessId)
            if (businessDetailsData.error.isEmpty()) {
                business.postValue(
                    Pair(
                        businessDetailsData.businessDetails?.categories.toString(),
                        businessDetailsData.businessDetails?.photos.toString()
                    )
                )
            } else {
                business.postValue(Pair(businessDetailsData.error, ""))

            }
        }
    }
}