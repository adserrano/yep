package com.puzzlebench.yeap_aac.presentation.BusinessDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepository

@Suppress("UNCHECKED_CAST")
class BusinessDetailsViewModelFactory(private val businessDetailsRepository: BusinessDetailsRepository) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>) =
        BusinessDetailsViewModel(
            businessDetailsRepository
        ) as T
}