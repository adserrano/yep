package com.puzzlebench.yeap_aac.presentation.BusinessList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.puzzlebench.yeap_aac.repository.BusinessRepository
import kotlinx.coroutines.launch

class BusinessViewModel internal constructor(private val businessRepository: BusinessRepository) :
    ViewModel() {

    val business: MutableLiveData<String> = MutableLiveData()

    fun getBusinessList() {
        viewModelScope.launch {
            val businessData = businessRepository.getBusiness()
            if (businessData.error.isEmpty()) {
                business.postValue(businessData.businesses[0].businessId)
            } else {
                business.postValue(businessData.error)

            }
        }
    }
}