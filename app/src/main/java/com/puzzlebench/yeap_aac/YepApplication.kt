package com.puzzlebench.yeap_aac

import android.app.Application
import com.puzzlebench.yeap_aac.presentation.di.ServiceLocator
import com.puzzlebench.yeap_aac.repository.BusinessDetailsRepository
import com.puzzlebench.yeap_aac.repository.BusinessRepository

class YepApplication : Application() {

    val businessRepository: BusinessRepository
        get() = ServiceLocator.provideBusinessRepository(this)

    val businessDetailsRepository: BusinessDetailsRepository
        get() = ServiceLocator.provideBusinessDetailsRepository(this)
}