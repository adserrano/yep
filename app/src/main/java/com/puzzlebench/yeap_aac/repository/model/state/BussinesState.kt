package com.puzzlebench.yeap_aac.repository.model.state

import com.puzzlebench.yeap_aac.repository.model.Business

const val NO_ERROR = ""

class BussinesState(
    val businesses: List<Business>,
    val error: String = NO_ERROR
)