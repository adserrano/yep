package com.puzzlebench.yeap_aac.repository.model.state

import com.puzzlebench.yeap_aac.repository.model.BusinessDetails
import com.puzzlebench.yeap_aac.repository.model.state.NO_ERROR

data class BusinessDetailsState(
    val businessDetails: BusinessDetails?,
    val error: String = NO_ERROR
)