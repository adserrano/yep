package com.puzzlebench.yeap_aac.repository.model

data class Business(
    val businessId: String,
    val name: String,
    val imageUrl: String,
    val displayPhone: String,
    val price: String
)
