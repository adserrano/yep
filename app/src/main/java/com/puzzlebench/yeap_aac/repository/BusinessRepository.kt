package com.puzzlebench.yeap_aac.repository

import com.puzzlebench.yeap_aac.repository.model.state.BussinesState

interface BusinessRepository {
    suspend fun getBusiness(): BussinesState
}
