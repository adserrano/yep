package com.puzzlebench.yeap_aac.repository

import com.puzzlebench.yeap_aac.data.local.BusinessLocalData
import com.puzzlebench.yeap_aac.data.local.BusinessLocalDataImpl
import com.puzzlebench.yeap_aac.data.network.FetchSwitzerlandBusinesses
import com.puzzlebench.yeap_aac.repository.model.state.BussinesState

class BusinessRepositoryImpl constructor(
    private val fetchSwitzerlandBusiness: FetchSwitzerlandBusinesses,
    private val businessLocalData: BusinessLocalData
) :
    BusinessRepository {

    override suspend fun getBusiness(): BussinesState {

        val localBusiness = businessLocalData.getBusiness()
        return if (localBusiness.businesses.isEmpty()) {
            val remoteBusiness = fetchSwitzerlandBusiness.fetchSwitzerlandBusiness()
            if (remoteBusiness.error.isEmpty()) {
                remoteBusiness.businesses.map {
                    businessLocalData.saveBusiness(it)
                }
            }
            remoteBusiness
        } else {
            localBusiness
        }
    }
}

