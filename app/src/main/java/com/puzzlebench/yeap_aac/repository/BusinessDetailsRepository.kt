package com.puzzlebench.yeap_aac.repository

import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState

interface BusinessDetailsRepository {
    suspend fun getBusinessDetailsById(businessId: String): BusinessDetailsState
}
