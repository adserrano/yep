package com.puzzlebench.yeap_aac.repository.model

class BusinessDetails(
    val businessId: String,
    val categories: List<String>,
    val photos: List<String>
)