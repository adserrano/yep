package com.puzzlebench.yeap_aac.repository

import com.puzzlebench.yeap_aac.data.local.BusinessDetailsLocalData
import com.puzzlebench.yeap_aac.data.network.FetchBusinessDetailsById
import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState

class BusinessDetailsRepositoryImpl constructor(
    private val fetchBusinessDetailsById: FetchBusinessDetailsById,
    private val businessDetailsLocalData: BusinessDetailsLocalData
) :
    BusinessDetailsRepository {
    override suspend fun getBusinessDetailsById(businessId: String): BusinessDetailsState {
        val localData = businessDetailsLocalData.getBusinessDetailsByBusinessId(businessId)
        if (localData.businessDetails == null) {
            val remoteData = fetchBusinessDetailsById.fetchBusinessDetailsState(businessId)
            if (remoteData.error.isEmpty()) {
                remoteData.businessDetails?.let {
                    businessDetailsLocalData.insertBusinessDetails(it)
                }
            }
            return remoteData
        }
        return localData
    }
}
