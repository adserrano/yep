package com.puzzlebench.yeap_aac.data.network

import com.puzzlebench.yeap_aac.repository.model.state.BussinesState

interface FetchSwitzerlandBusinesses {
    suspend fun fetchSwitzerlandBusiness(): BussinesState
}