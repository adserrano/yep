package com.puzzlebench.yeap_aac.data.network

import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState

interface FetchBusinessDetailsById {
    suspend fun fetchBusinessDetailsState(businessId: String): BusinessDetailsState
}