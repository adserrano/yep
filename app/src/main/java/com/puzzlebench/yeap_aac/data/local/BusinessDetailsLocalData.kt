package com.puzzlebench.yeap_aac.data.local

import com.puzzlebench.yeap_aac.repository.model.BusinessDetails
import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState

interface BusinessDetailsLocalData {

    suspend fun getBusinessDetailsByBusinessId(businessId: String): BusinessDetailsState

    suspend fun insertBusinessDetails(business: BusinessDetails)

    suspend fun deleteAllBusinessDetails()
}
