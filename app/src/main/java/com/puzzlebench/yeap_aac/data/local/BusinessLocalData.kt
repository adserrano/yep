package com.puzzlebench.yeap_aac.data.local

import com.puzzlebench.yeap_aac.repository.model.Business
import com.puzzlebench.yeap_aac.repository.model.state.BussinesState

interface BusinessLocalData {
    suspend fun getBusiness(): BussinesState
    suspend fun saveBusiness(business: Business)
    suspend fun deleteAll()

}