package com.puzzlebench.yeap_aac.data.network

import com.puzzlebench.yeap_aac.data.mapper.BusinessDetailMapper
import com.puzzlebench.yeap_aac.data.network.retrofit.YelpApiV3
import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FetchBusinessDetailsByIdImpl(
    private val api: YelpApiV3,
    private val mapper: BusinessDetailMapper
) : FetchBusinessDetailsById {
    override suspend fun fetchBusinessDetailsState(businessId: String): BusinessDetailsState =
        withContext(
            Dispatchers.IO
        ) {
            return@withContext try {
                val response = api.getBusinessDetailsById(businessId)
                val businessDetails = mapper.transformServiceToRepository(response)
                BusinessDetailsState(
                    businessDetails
                )
            } catch (ex: Exception) {
                BusinessDetailsState(
                    null,
                    ex.message.toString()
                )
            }
        }
}