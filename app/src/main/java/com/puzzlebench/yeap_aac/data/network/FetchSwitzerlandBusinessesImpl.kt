package com.puzzlebench.yeap_aac.data.network

import com.puzzlebench.yeap_aac.repository.model.state.BussinesState
import com.puzzlebench.yeap_aac.data.mapper.BusinessMapper
import com.puzzlebench.yeap_aac.data.network.retrofit.YelpApiV3
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class FetchSwitzerlandBusinessesImpl(
    private val api: YelpApiV3,
    private val mapper: BusinessMapper
) : FetchSwitzerlandBusinesses {
    override suspend fun fetchSwitzerlandBusiness(): BussinesState = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = api.getSwitzerlandBusiness().let { baseResponse ->
                baseResponse.businesses.map {
                    mapper.transformServiceToRepository(it)
                }
            }
            BussinesState(response)
        } catch (ex: Exception) {
            BussinesState(
                listOf(),
                ex.message.toString()
            )
        }
    }

}