package com.puzzlebench.yeap_aac.data.network.retrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    companion object FactoryYelpApi {

        private val PRIVATE_API_KEY_ARG = "Authorization"
        private val PRIVATE_API_KEY_VALUE =
            "Bearer r37kmDrx_nmpeR46HeU6JZh0NeB66Vtblz8yY-7TMCbq9L8TfGGjSciWiM4cXLazT4pfXrGAZL_OwbkXvL1hadp-e0pJHbfJ7ReBQOko-WBVJxr2IfnFk25BRcHAXXYx"

        private fun getOkHttpClientBuilder(): OkHttpClient.Builder {

            return OkHttpClient.Builder().addInterceptor { chain ->
                val defaultRequest = chain.request()
                val headers = defaultRequest.headers().newBuilder()
                    .add(
                        PRIVATE_API_KEY_ARG,
                        PRIVATE_API_KEY_VALUE
                    ).build()
                val requestBuilder = defaultRequest.newBuilder().headers(headers)
                chain.proceed(requestBuilder.build())
            }
        }

        fun makeServiceYelpApiV3(): YelpApiV3 {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClientBuilder().build())
                .baseUrl("https://api.yelp.com/")
                .build()
            return retrofit.create(YelpApiV3::class.java)
        }
    }
}