package com.puzzlebench.yeap_aac.data.mapper

import com.puzzlebench.yeap_aac.data.local.room.entity.CategoriesEntity
import com.puzzlebench.yeap_aac.data.local.room.entity.PhotoEntity
import com.puzzlebench.yeap_aac.repository.model.BusinessDetails
import com.puzzlebench.yeap_aac.data.network.BusinessDetailResponse
import com.puzzlebench.yeap_aac.data.network.CategoriesResponse

class BusinessDetailMapper {

    fun transformServiceToRepository(service: BusinessDetailResponse) =
        BusinessDetails(
            service.businessId,
            getCategories(service.categories),
            service.photos
        )

    fun transformRepositoryToEntity(businessId: String, title: String) =
        CategoriesEntity(0, businessId, title)

    private fun getCategories(categoriesResponse: List<CategoriesResponse>): List<String> =
        categoriesResponse.map { it.title }

    fun transformPhotoRepositoryToEntity(businessId: String, url: String) =
        PhotoEntity(0, businessId, url)

    fun transformEntityToRepository(
        categoriesEntity: List<CategoriesEntity>,
        photoEntity: List<PhotoEntity>
    ) =
        BusinessDetails(
            categoriesEntity.first().businessId,
            categoriesEntity.map { it.title },
            photoEntity.map { it.photoUrl }
        )
}