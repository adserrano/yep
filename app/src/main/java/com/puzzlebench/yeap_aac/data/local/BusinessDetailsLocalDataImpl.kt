package com.puzzlebench.yeap_aac.data.local

import com.puzzlebench.yeap_aac.data.local.room.dao.CategoriesDao
import com.puzzlebench.yeap_aac.data.local.room.dao.PhotoDao
import com.puzzlebench.yeap_aac.data.mapper.BusinessDetailMapper
import com.puzzlebench.yeap_aac.repository.model.BusinessDetails
import com.puzzlebench.yeap_aac.repository.model.state.BusinessDetailsState
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BusinessDetailsLocalDataImpl constructor(
    private val categoriesDao: CategoriesDao,
    private val photoDao: PhotoDao,
    private val mapper: BusinessDetailMapper,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : BusinessDetailsLocalData {
    override suspend fun getBusinessDetailsByBusinessId(businessId: String): BusinessDetailsState =
        withContext(ioDispatcher) {
            return@withContext try {
                val resultCategory = categoriesDao.getCategoriesByBusinessId(businessId)
                val resultPhoto = photoDao.getPhotosByBusinessId(businessId)
                BusinessDetailsState(
                    mapper.transformEntityToRepository(
                        resultCategory,
                        resultPhoto
                    ), ""
                )

            } catch (e: Exception) {
                BusinessDetailsState(null, e.message ?: "")
            }
        }

    override suspend fun insertBusinessDetails(business: BusinessDetails) {
        business.categories.map {
            categoriesDao.insertCategories(
                mapper.transformRepositoryToEntity(
                    business.businessId,
                    it
                )
            )
        }
        business.photos.map {
            photoDao.insertPhoto(
                mapper.transformPhotoRepositoryToEntity(
                    business.businessId,
                    it
                )
            )
        }
    }

    override suspend fun deleteAllBusinessDetails() = withContext(ioDispatcher) {
        photoDao.deleteAllPhotos()
        categoriesDao.deleteAllCategories()
    }

}