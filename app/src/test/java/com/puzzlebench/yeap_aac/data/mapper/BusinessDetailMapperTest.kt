package com.puzzlebench.yeap_aac.data.mapper

import com.puzzlebench.yeap_aac.DummyBusinessDetailsFactory.getDummyBusinessDetailResponse
import com.puzzlebench.yeap_aac.DummyBusinessDetailsFactory.getDummyListCategoriesEntity
import com.puzzlebench.yeap_aac.DummyBusinessDetailsFactory.getDummyListPhotosEntity
import com.puzzlebench.yeap_aac.data.local.room.entity.CategoriesEntity
import com.puzzlebench.yeap_aac.data.local.room.entity.PhotoEntity
import com.puzzlebench.yeap_aac.data.network.BusinessDetailResponse
import com.puzzlebench.yeap_aac.repository.model.BusinessDetails
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class BusinessDetailMapperTest {

    private lateinit var mapper: BusinessDetailMapper

    @Before
    fun setUp() {
        mapper = BusinessDetailMapper()
    }

    @Test
    fun transformEntityToRepository() {
        val categoriesEntity = getDummyListCategoriesEntity()
        val photoEntity = getDummyListPhotosEntity()
        val result = mapper.transformEntityToRepository(categoriesEntity, photoEntity)
        result.categories.forEachIndexed { index, s ->
            assertEquals(categoriesEntity[index].title, s)
        }
        result.photos.forEachIndexed { index, s ->
            assertEquals(photoEntity[index].photoUrl, s)
        }

    }

    @Test
    fun transformServiceToRepository() {
        val response = getDummyBusinessDetailResponse("1")
        val result = mapper.transformServiceToRepository(response)
        assertDataEquality(response, result)
    }

    private fun assertDataEquality(response: BusinessDetailResponse, repository: BusinessDetails) {
        assertEquals(repository.businessId, response.businessId)
        response.categories.forEachIndexed { index, responseTitle ->
            assertEquals(repository.categories[index], responseTitle.title)
        }
        response.photos.forEachIndexed { index, responseTitle ->
            assertEquals(repository.photos[index], responseTitle)
        }
    }

}