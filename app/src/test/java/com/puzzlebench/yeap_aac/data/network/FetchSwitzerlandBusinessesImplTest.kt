package com.puzzlebench.yeap_aac.data.network

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.puzzlebench.yeap_aac.DummyBusinessFactory.getDummyYepResponse
import com.puzzlebench.yeap_aac.data.mapper.BusinessMapper
import com.puzzlebench.yeap_aac.data.network.retrofit.YelpApiV3
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class FetchSwitzerlandBusinessesImplTest {
    private lateinit var fetchSwitzerlandBusinesses: FetchSwitzerlandBusinessesImpl
    private val serviceResponse = getDummyYepResponse()
    private val service = mock<YelpApiV3> {
        onBlocking { getSwitzerlandBusiness() } doReturn serviceResponse
    }
    private var businessMapper = mock<BusinessMapper>()

    @Before
    fun setUp() {
        fetchSwitzerlandBusinesses = FetchSwitzerlandBusinessesImpl(service, businessMapper)
    }

    @Test
    fun getSwitzerlandBusiness() {
        runBlocking {
            fetchSwitzerlandBusinesses.fetchSwitzerlandBusiness()
            verify(service).getSwitzerlandBusiness()
            serviceResponse.businesses.forEach {
                verify(businessMapper).transformServiceToRepository(it)
            }
        }
    }

}