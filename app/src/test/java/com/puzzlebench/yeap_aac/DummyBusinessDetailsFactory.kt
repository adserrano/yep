package com.puzzlebench.yeap_aac

import com.puzzlebench.yeap_aac.data.local.room.entity.CategoriesEntity
import com.puzzlebench.yeap_aac.data.local.room.entity.PhotoEntity
import com.puzzlebench.yeap_aac.data.network.BusinessDetailResponse
import com.puzzlebench.yeap_aac.data.network.CategoriesResponse

object DummyBusinessDetailsFactory {
    const val BUSINESS_ID = "BUSINESS_ID"
    const val CATEGORY_TITLE = "category title"
    const val URL_PHOTO = "www.photo.com"


    fun getDummyBusinessDetailResponse(seed: String) = BusinessDetailResponse(
        "${BUSINESS_ID}$seed",
        getDummyListCategoriesResponse(),
        getDummyListPhotoResponse()
    )


    private fun getDummyCategoriesResponse(seed: String) =
        CategoriesResponse("$CATEGORY_TITLE$seed")


    private fun getDummyListCategoriesResponse(): List<CategoriesResponse> = (1..20).map {
        getDummyCategoriesResponse(it.toString())
    }

    private fun getDummyListPhotoResponse(): List<String> = (1..20).map {
        "$URL_PHOTO$it"
    }

    private fun getDummyCategoriesEntity(seed: String) =
        CategoriesEntity(
            0,
            "${BUSINESS_ID}$seed",
            "$CATEGORY_TITLE$seed"

        )

    private fun getDummyPhotosEntity(seed: String) =
        PhotoEntity(
            0,
            "${BUSINESS_ID}$seed",
            "$URL_PHOTO$seed"

        )

    fun getDummyListCategoriesEntity(): List<CategoriesEntity> = (1..20).map {
        getDummyCategoriesEntity(it.toString())
    }

    fun getDummyListPhotosEntity(): List<PhotoEntity> = (1..20).map {
        getDummyPhotosEntity(it.toString())
    }

}