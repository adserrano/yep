package com.puzzlebench.yeap_aac.repository

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.puzzlebench.yeap_aac.DummyBusinessFactory.getBussinesStateEmpty
import com.puzzlebench.yeap_aac.DummyBusinessFactory.getBussinesStateError
import com.puzzlebench.yeap_aac.DummyBusinessFactory.getBussinesStateNoError
import com.puzzlebench.yeap_aac.data.local.BusinessLocalData
import com.puzzlebench.yeap_aac.data.network.FetchSwitzerlandBusinesses
import kotlinx.coroutines.runBlocking
import org.junit.Test

class BusinessRepositoryImplTest {

    private lateinit var businessRepositoryImpl: BusinessRepositoryImpl


    @Test
    fun `test empty local data`() {
        val serviceResponse = getBussinesStateNoError()
        val fetchSwitzerlandBusinesses = mock<FetchSwitzerlandBusinesses> {
            onBlocking { fetchSwitzerlandBusiness() } doReturn serviceResponse
        }

        val businessLocalData = mock<BusinessLocalData> {
            onBlocking { getBusiness() } doReturn getBussinesStateEmpty()
        }
        businessRepositoryImpl =
            BusinessRepositoryImpl(fetchSwitzerlandBusinesses, businessLocalData)
        runBlocking {
            businessRepositoryImpl.getBusiness()
            verify(businessLocalData).getBusiness()
            verify(fetchSwitzerlandBusinesses).fetchSwitzerlandBusiness()
            serviceResponse.businesses.forEach {
                verify(businessLocalData).saveBusiness(it)

            }
        }
    }

    @Test
    fun `test get local data`() {
        val fetchSwitzerlandBusinesses = mock<FetchSwitzerlandBusinesses>()
        val businessLocalData = mock<BusinessLocalData> {
            onBlocking { getBusiness() } doReturn getBussinesStateNoError()
        }
        businessRepositoryImpl =
            BusinessRepositoryImpl(fetchSwitzerlandBusinesses, businessLocalData)
        runBlocking {
            businessRepositoryImpl.getBusiness()
            verify(businessLocalData).getBusiness()
        }
    }

    @Test
    fun `test error getting local data`() {
        val serviceResponse = getBussinesStateError()
        val fetchSwitzerlandBusinesses = mock<FetchSwitzerlandBusinesses> {
            onBlocking { fetchSwitzerlandBusiness() } doReturn serviceResponse
        }

        val businessLocalData = mock<BusinessLocalData> {
            onBlocking { getBusiness() } doReturn getBussinesStateError()
        }
        businessRepositoryImpl =
            BusinessRepositoryImpl(fetchSwitzerlandBusinesses, businessLocalData)
        runBlocking {
            businessRepositoryImpl.getBusiness()
            verify(businessLocalData).getBusiness()
            verify(fetchSwitzerlandBusinesses).fetchSwitzerlandBusiness()
        }
    }
}